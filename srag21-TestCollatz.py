#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "2 20\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  2)
        self.assertEqual(j, 20)
    
    def test_read_3(self):
        s = "3 30\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  3)
        self.assertEqual(j, 30)

    def test_read_4(self):
        s = "4 40\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  4)
        self.assertEqual(j, 40)


    # ----
    # eval
    # ----

    # def test_eval_1(self):
    #     v = collatz_eval(1, 10)
    #     self.assertEqual(v, 1)

    # def test_eval_2(self):
    #     v = collatz_eval(100, 200)
    #     self.assertEqual(v, 1)

    # def test_eval_3(self):
    #     v = collatz_eval(201, 210)
    #     self.assertEqual(v, 1)

    # def test_eval_4(self):
    #     v = collatz_eval(900, 1000)
    #     self.assertEqual(v, 1)

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(956739, 956006)
        self.assertEqual(v, 352)
    
    def test_eval_6(self):
        v = collatz_eval(815892, 815933)
        self.assertEqual(v, 269)
    
    def test_eval_7(self):
        v = collatz_eval(748121, 748121)
        self.assertEqual(v, 256)


    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
    
    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 2, 20, 21)
        self.assertEqual(w.getvalue(), "2 20 21\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 3, 30, 112)
        self.assertEqual(w.getvalue(), "3 30 112\n")
    
    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 4, 40, 112)
        self.assertEqual(w.getvalue(), "4 40 112\n")


    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            #w.getvalue(), "1 10 1\n100 200 1\n201 210 1\n900 1000 1\n")
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
        
    def test_solve_2(self):
        r = StringIO("2 20\n3 30\n4 40\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "2 20 21\n3 30 112\n4 40 112\n")
    
    def test_solve_3(self):
        r = StringIO("5 50\n6 60\n7 70\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "5 50 112\n6 60 113\n7 70 113\n")
    
    def test_solve_4(self):
        r = StringIO("8 80\n9 90\n10 100\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "8 80 116\n9 90 116\n10 100 119\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
